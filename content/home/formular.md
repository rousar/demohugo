+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = 2016-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Kontaktní formulář"
subtitle = "k odběru Newsletteru"

# Order that this section will appear in.
weight = 200

+++

Pokud chcete odebírat náš newsletter, prosím zadejte do formuláře Vaši emailovou adresu
<script type="text/javascript" src="//crm.urad.online/form/generate.js?id=3"></script>